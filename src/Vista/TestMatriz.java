/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.MatrizEntero;
import java.util.Scanner;

/**
 * Clase para probar el uso de matriz entero
 *
 * @author madarme
 */
public class TestMatriz {

    public static void main(String[] args) {
        try {
            MatrizEntero m = new MatrizEntero(leerEntero("Digite cantidad de filas"), leerEntero("cantidad de columnas"));
            System.out.println(m.toString());
            MatrizEntero m2 = new MatrizEntero(leerEntero("Dato para una matriz dispersa, por favor digite la cantidad de filas"));
            System.out.println("Matriz dispersa");
            System.out.println(m2.toString());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private static int leerEntero(String mensaje) {
        Scanner myObj = new Scanner(System.in);  // Create a Scanner object
        System.out.println(mensaje);
        try {
            return (myObj.nextInt());
        } catch (Exception e) { //java.util.InputMismatchException
            System.out.println("Dato incorrecto");
            return (leerEntero(mensaje));
        }
    }

}
